import React from 'react';
import { Text, View, StyleSheet, FlatList, Dimensions } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const collections = [
    {
        'id': 1,
        'icon': 'book-open',
        'text': 'Fairy Tale'
    },
    {
        'id': 2,
        'icon': 'clipboard',
        'text': 'Funny Story'
    },
    {
        'id': 3,
        'icon': 'star',
        'text': 'Favorite'
    },
    {
        'id': 4,
        'icon': 'grid',
        'text': 'More'
    }
]

export default function Collection() {

    return (
        <View style={styles.collections}>
            {collections.map((item) => {
                return <View key={item.id} style={styles.collection}>
                    <View style={styles.collectionIcon}>
                        <Feather name={item.icon} size={30} />
                    </View>
                    <Text style={styles.collectionName}>{item.text}</Text>
                </View>
            })}
        </View>
    )
}


const styles = StyleSheet.create({
    collections: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        maxWidth: WIDTH - 40,
        maxHeight: HEIGHT / 8,
    },
    collection: {
        padding: 13
    },
    collectionIcon: {
        alignItems: 'center'
    },
    collectionName: {
        marginTop: 8,
        fontSize: 13
    }
})