import React from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Feather from 'react-native-vector-icons/Feather';
// components
import MainScreen from './screens/MainScreen';
import Personal from './screens/Personal';

const Tab = createBottomTabNavigator();

export default function TabNavigator() {
    return (
        <NavigationContainer>
            <Tab.Navigator
                initialRouteName="Home"
                tabBarOptions={{
                    activeTintColor: '#e91e63',
                    style: styles.tabNavigator
                }}
            >
                <Tab.Screen
                    name="Home"
                    component={MainScreen}
                    options={{
                        tabBarLabel: 'Home',
                        tabBarIcon: ({ color, size }) => (
                            <Feather name={'home'} size={20} />
                        ),
                    }}
                />
                <Tab.Screen
                    name="Personal"
                    component={Personal}
                    options={{
                        tabBarLabel: 'Personal',
                        tabBarIcon: ({ color, size }) => (
                            <Feather name={'user'} size={20} />
                        ),
                    }}
                />
            </Tab.Navigator>
        </NavigationContainer>
    )
}


const styles = StyleSheet.create({
    tabNavigator: {
        borderTopLeftRadius: 19,
        borderTopRightRadius: 19,
        position: 'absolute',
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.50,
        shadowRadius: 4.65
    }
});
