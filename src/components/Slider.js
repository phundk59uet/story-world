import React, { useState } from 'react';
import { Text, View, Image, Dimensions, ScrollView, StyleSheet, SafeAreaView } from 'react-native';

const images = [
    'https://3.bp.blogspot.com/-XdOmzGszwUQ/RiBl1P20rxI/AAAAAAAAACo/Gb8TlO4Oqxc/s1600/boatinthetree2930.jpg',
    'https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/6b768a35887461.56062e926b5c8.jpg',
    'https://artrep1.files.wordpress.com/2013/01/bluejay1.jpg'
]

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default function Slider() {
    const [active, setActive] = useState(0);

    const handleScroll = (nativeEvent) => {
        if (nativeEvent) {
            const slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width);
            slide !== active && setActive(slide);
        }
    }

    return (
        <View style={styles.slider}>
            <ScrollView
                pagingEnabled
                horizontal
                showsHorizontalScrollIndicator={false}
                scrollEventThrottle={32}
                onScroll={({ nativeEvent }) => handleScroll(nativeEvent)}
                style={styles.scrollView}
            >
                {images.map((e, index) => {
                    return <Image
                        key={index}
                        resizeMode='stretch'
                        source={{ uri: e }}
                        style={styles.image}
                    />
                })}
            </ScrollView>
            <View style={styles.dotView}>
                {images.map((e, index) =>
                    <Text key={index} style={index === active ? styles.dotAtive : styles.dot}> ●</Text>
                )}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    slider: {
        height: HEIGHT / 4,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65
    },
    scrollView: {
        borderRadius: 15
    },
    image: {
        width: WIDTH - 40,
        height: HEIGHT / 4,
        borderRadius: 15
    },
    dotView: {
        position: 'absolute',
        bottom: 0,
        flexDirection: 'row',
        alignSelf: 'center'
    },
    dot: {
        color: 'white'
    },
    dotAtive: {
        color: 'black'
    }
})