import React from 'react';
import { Text, View, TextInput, StyleSheet } from 'react-native';

export default function Search() {
    return (
        <TextInput
            placeholder={'Search...'}
            placeholderTextColor='#989898'
            keyboardType='numeric'
            style={styles.input}
        />
    )
}

const styles = StyleSheet.create({
    input: {
        borderWidth: 0,
        borderRadius: 15,
        backgroundColor: '#fff',
        marginBottom: 20,
        paddingLeft: 12,
        paddingTop: 10,
        paddingBottom: 10
    }
})