import React from 'react';
import { Text, View, FlatList, StyleSheet } from 'react-native';
// components
import Item from './Item';


const items = [
    {
        id: 1,
        name: 'Tấm cám',
        description: 'Truyện cổ tích Việt Nam',
        image: 'https://salt.tikicdn.com/media/catalog/product/t/a/tam-cam.u547.d20170120.t100949.386890.jpg',
        read: 456456
    },
    {
        id: 2,
        name: 'Sự tích cây khế',
        description: 'Truyện cổ tích Việt Nam',
        image: 'https://salt.tikicdn.com/media/catalog/product/t/a/tam-cam.u547.d20170120.t100949.386890.jpg',
        read: 4398579
    },
    {
        id: 3,
        name: 'Sự tích cây chuối',
        description: 'Truyện cổ tích Việt Nam',
        image: 'https://salt.tikicdn.com/media/catalog/product/t/a/tam-cam.u547.d20170120.t100949.386890.jpg',
        read: 10789780
    }
]


export default function ItemList() {
    const renderItem = ({ item }) => {

        return (
            <Item item={item} />
        );
    };

    return <View style={styles.itemList}>
        <FlatList
            data={items}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            style={styles.itemList}
        />
    </View>
}


const styles = StyleSheet.create({
    itemList: {
        backgroundColor: '#f4f4f4'
    }
})