import React from 'react';
import { Text, View, StyleSheet, Image, Pressable, Dimensions } from 'react-native';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default function Item({ item }) {
    return <View style={styles.card}>
        <Image source={{ uri: item.image }} style={styles.image} />
        <View style={styles.content}>
            <Text style={styles.itemName}>{item.name}</Text>
            <Text style={styles.itemDescription}>{item.description}</Text>
            <View style={styles.itemFooter}>
                <Text style={styles.itemFooterView}>{item.read} views</Text>
                <Pressable style={styles.itemFooterBtn}>
                    <Text>View</Text>
                </Pressable>
            </View>
        </View>
    </View>
}

const styles = StyleSheet.create({
    card: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 15,
        marginBottom: 15,
        padding: 10,
        backgroundColor: '#ffffff'
    },
    image: {
        height: 75,
        width: (WIDTH - 40) / 3,
        flex: .4
    },
    content: {
        flex: .8,
        paddingLeft: 12
    },
    itemName: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 10
    },
    itemDescription: {
        marginBottom: 5,
        color: '#a8a8a8'
    },
    itemFooter: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    itemFooterView: {
        flex: 1,
        color: '#a8a8a8'
    },
    itemFooterBtn: {
        flex: 0,
        backgroundColor: '#f8ce58',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 6,
        paddingHorizontal: 30,
        borderBottomLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20
    }
})