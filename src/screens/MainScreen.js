import React from 'react';
import { ScrollView, View, StyleSheet, Dimensions } from 'react-native';
// components
import Slider from '../components/Slider';
import Search from '../components/Search';
import Collection from '../components/Collection';
import ItemList from '../components/ItemList';

const HEIGHT = Dimensions.get('window').height;

export default function MainScreen() {
    return (
        <ScrollView
            showsVerticalScrollIndicator={false}
        >
            <View style={styles.mainScreen}>
                <Search />
                <Slider />
                <Collection />
                <ItemList />
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    mainScreen: {
        padding: 20,
        backgroundColor: '#f4f4f4',
        height: HEIGHT,
        zIndex: -1
    }
})