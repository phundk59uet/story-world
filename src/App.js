/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView
} from 'react-native';
// screens
import TabNavigator from './Navigator';


const App = () => {

  return (
    <React.Fragment>
      <SafeAreaView />
      <TabNavigator />
    </React.Fragment>
  );
};

export default App;
